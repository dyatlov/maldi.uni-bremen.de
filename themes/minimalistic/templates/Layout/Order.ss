<h1>$Title</h1>
<div>
	Order Status: <span class="order-status">$OrderStatus</span>
</div>
<div>
	Expected time of completion: <span class="estimated-time">$EstimatedTime</span>
</div>
$Content $Form
