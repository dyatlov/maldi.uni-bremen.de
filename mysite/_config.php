<?php
global $project;
$project = 'mysite';

global $databaseConfig;
$domain = preg_replace("/^www\./", "", $_SERVER['HTTP_HOST']);
if ($domain == "maldi.uni-bremen.de") {
	Director::set_environment_type("live");
	$databaseConfig = array(
		"type" => 'MySQLDatabase',
		"server" => 'sqlhost1.informatik.uni-bremen.de',
		"username" => 'dyatlov',
		"password" => 'pTwLBxbK',
		"database" => 'maldi_imaging',
		"path" => '',
	);
} else {
	ini_set("log_errors", "On");
	ini_set("error_log", "/home/dyatlov/temp/phplog.txt");	
	SS_Log::add_writer(new SS_LogFileWriter('/home/dyatlov/temp/log.txt'), SS_Log::WARN, '<=');
	Director::set_environment_type("dev");
	Security::setDefaultAdmin('admin','admin');
	$databaseConfig = array(
		"type" => 'MySQLDatabase',
		"server" => 'localhost',
		"username" => 'root',
		"password" => '',
		"database" => 'maldi',
		"path" => '',
	);
}
MySQLDatabase::set_connection_charset('utf8');

// This line set's the current theme. More themes can be
// downloaded from http://www.silverstripe.org/themes/
SSViewer::set_theme('minimalistic');

// Set the site locale
i18n::set_locale('en_US');

Object::add_extension('SiteConfig', 'CustomSiteConfig');

// enable nested URLs for this site (e.g. page/sub-page/)
SiteTree::enable_nested_urls();
