<?php
class Orders extends Page {

	public static $db = array(
	);

	public static $has_one = array(
	);
	
	static $defaults = array(
		"ShowInMenus" => 0,
		"ShowInSearch" => 0,
	);

	static $allowed_children = array("Order");

	function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->removeByName("Behavior");
		$fields->removeByName("Access");
		$fields->removeByName("GoogleSitemap");
		$fields->removeFieldsFromTab("Root.Main", array("MenuTitle", "Content"));

		return $fields;
	}
}
class Orders_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	public static $allowed_actions = array (
	);

	public function init() {
		parent::init();
	}
}