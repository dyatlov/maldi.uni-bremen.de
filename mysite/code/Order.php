<?php
class Order extends Page {

	public static $db = array(
		'CustomerName' => 'Varchar(100)',
		'CustomerEmail' => 'Varchar(256)',
		'OrderStatus' => "Enum('Delivered, In Preparation, Measurement Running, Data Analysis in Progress, Finished', 'Delivered')",
		'EstimatedTime' => 'Date');

	public static $has_one = array(
	);

	static $default_parent = "Orders";
	static $can_be_root = false;
	static $allowed_children = null;


	static $defaults = array(
		"ShowInMenus" => 0,
		"ShowInSearch" => 0,
	);

	protected function onBeforeWrite() {
		if((!$this->URLSegment || $this->URLSegment == 'new-order')) {
			$this->URLSegment = uniqid();
		}
		parent::onBeforeWrite();
	}

	function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->removeByName("Metadata");
		$fields->removeByName("Behavior");
		$fields->removeByName("Access");
		$fields->removeByName("GoogleSitemap");
		$fields->removeFieldFromTab("Root.Main", "MenuTitle");

		$fields->addFieldToTab("Root.Main",
		new FieldGroup(_t('Order.PAGE_URL', 'URL'), new LabelField("URL", $this->AbsoluteLink())), 'Content');
		$fields->addFieldToTab("Root.Main", new DropdownField('OrderStatus', 'Order Status', singleton('Order')->dbObject('OrderStatus')->enumValues()), 'Content');
		$fields->addFieldToTab('Root.Main', $dateF = new DateField('EstimatedTime', _t('Order.EXPECTED_TIME', 'Expected time of completion')), "Content");
		$fields->addFieldToTab('Root.Main', new TextField('CustomerName', _t('Order.CUSTOMER_NAME', 'Customer Name'), '', 100), "Content");
		$fields->addFieldToTab('Root.Main', new EmailField('CustomerEmail', _t('Order.CUSTOMER_NAME', 'Customer Email'), '', 256), "Content");
		$dateF->setConfig('showcalendar', true);
		return $fields;
	}
	function ExtraMeta() {
		return '<meta name="robots" content="noindex,nofollow">';
	}
}
class Order_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	public static $allowed_actions = array (
	);

	public function init() {
		parent::init();
	}
}